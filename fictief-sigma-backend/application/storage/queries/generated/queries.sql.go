// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0
// source: queries.sql

package queries

import (
	"context"
	"encoding/json"

	"github.com/google/uuid"
	"github.com/lib/pq"
)

const attributeCreate = `-- name: AttributeCreate :exec
INSERT INTO sigma.process_attributes
(id, process_id, attribute, value)
VALUES (
    $1, $2, $3, $4
)
`

type AttributeCreateParams struct {
	ID        uuid.UUID
	ProcessID uuid.UUID
	Attribute string
	Value     json.RawMessage
}

func (q *Queries) AttributeCreate(ctx context.Context, arg *AttributeCreateParams) error {
	_, err := q.exec(ctx, q.attributeCreateStmt, attributeCreate,
		arg.ID,
		arg.ProcessID,
		arg.Attribute,
		arg.Value,
	)
	return err
}

const attributeList = `-- name: AttributeList :many
SELECT id, process_id, attribute, value, created_at, deleted_at
FROM sigma.process_attributes
WHERE process_id=$1 AND attribute = ANY($2::TEXT[])
`

type AttributeListParams struct {
	ProcessID uuid.UUID
	Column2   []string
}

func (q *Queries) AttributeList(ctx context.Context, arg *AttributeListParams) ([]*SigmaProcessAttribute, error) {
	rows, err := q.query(ctx, q.attributeListStmt, attributeList, arg.ProcessID, pq.Array(arg.Column2))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*SigmaProcessAttribute{}
	for rows.Next() {
		var i SigmaProcessAttribute
		if err := rows.Scan(
			&i.ID,
			&i.ProcessID,
			&i.Attribute,
			&i.Value,
			&i.CreatedAt,
			&i.DeletedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const attributeSoftDelete = `-- name: AttributeSoftDelete :exec
UPDATE sigma.process_attributes
SET deleted_at=NOW()
WHERE id=$1
`

func (q *Queries) AttributeSoftDelete(ctx context.Context, id uuid.UUID) error {
	_, err := q.exec(ctx, q.attributeSoftDeleteStmt, attributeSoftDelete, id)
	return err
}

const observationCreate = `-- name: ObservationCreate :exec
INSERT INTO sigma.observations
(id, vreemdeling_id, attribute, value)
VALUES (
    $1, $2, $3, $4
)
`

type ObservationCreateParams struct {
	ID            uuid.UUID
	VreemdelingID string
	Attribute     string
	Value         json.RawMessage
}

func (q *Queries) ObservationCreate(ctx context.Context, arg *ObservationCreateParams) error {
	_, err := q.exec(ctx, q.observationCreateStmt, observationCreate,
		arg.ID,
		arg.VreemdelingID,
		arg.Attribute,
		arg.Value,
	)
	return err
}

const observationGet = `-- name: ObservationGet :one
SELECT id, vreemdeling_id, attribute, value, created_at, deleted_at
FROM sigma.observations
WHERE id=$1
`

func (q *Queries) ObservationGet(ctx context.Context, id uuid.UUID) (*SigmaObservation, error) {
	row := q.queryRow(ctx, q.observationGetStmt, observationGet, id)
	var i SigmaObservation
	err := row.Scan(
		&i.ID,
		&i.VreemdelingID,
		&i.Attribute,
		&i.Value,
		&i.CreatedAt,
		&i.DeletedAt,
	)
	return &i, err
}

const observationInvalidate = `-- name: ObservationInvalidate :exec
UPDATE sigma.observations
SET deleted_at=NOW()
WHERE id=$1
`

func (q *Queries) ObservationInvalidate(ctx context.Context, id uuid.UUID) error {
	_, err := q.exec(ctx, q.observationInvalidateStmt, observationInvalidate, id)
	return err
}

const observationList = `-- name: ObservationList :many
SELECT id, vreemdeling_id, attribute, value, created_at, deleted_at
FROM sigma.observations
WHERE vreemdeling_id=$1 AND attribute = ANY($2::TEXT[])
`

type ObservationListParams struct {
	VreemdelingID string
	Column2       []string
}

func (q *Queries) ObservationList(ctx context.Context, arg *ObservationListParams) ([]*SigmaObservation, error) {
	rows, err := q.query(ctx, q.observationListStmt, observationList, arg.VreemdelingID, pq.Array(arg.Column2))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*SigmaObservation{}
	for rows.Next() {
		var i SigmaObservation
		if err := rows.Scan(
			&i.ID,
			&i.VreemdelingID,
			&i.Attribute,
			&i.Value,
			&i.CreatedAt,
			&i.DeletedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const processCount = `-- name: ProcessCount :one
SELECT COUNT(0) FROM sigma.processes
`

func (q *Queries) ProcessCount(ctx context.Context) (int64, error) {
	row := q.queryRow(ctx, q.processCountStmt, processCount)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const processCreate = `-- name: ProcessCreate :exec
INSERT INTO sigma.processes
(id, process_id, vreemdeling_id, type, status)
VALUES (
    $1, $2, $3, $4, $5
)
`

type ProcessCreateParams struct {
	ID            uuid.UUID
	ProcessID     string
	VreemdelingID string
	Type          string
	Status        string
}

func (q *Queries) ProcessCreate(ctx context.Context, arg *ProcessCreateParams) error {
	_, err := q.exec(ctx, q.processCreateStmt, processCreate,
		arg.ID,
		arg.ProcessID,
		arg.VreemdelingID,
		arg.Type,
		arg.Status,
	)
	return err
}

const processGetID = `-- name: ProcessGetID :one
SELECT id, process_id, vreemdeling_id, type, status, created_at, deleted_at
FROM sigma.processes
WHERE id=$1
`

func (q *Queries) ProcessGetID(ctx context.Context, id uuid.UUID) (*SigmaProcess, error) {
	row := q.queryRow(ctx, q.processGetIDStmt, processGetID, id)
	var i SigmaProcess
	err := row.Scan(
		&i.ID,
		&i.ProcessID,
		&i.VreemdelingID,
		&i.Type,
		&i.Status,
		&i.CreatedAt,
		&i.DeletedAt,
	)
	return &i, err
}

const processGetPID = `-- name: ProcessGetPID :one
SELECT id, process_id, vreemdeling_id, type, status, created_at, deleted_at
FROM sigma.processes
WHERE process_id=$1
`

func (q *Queries) ProcessGetPID(ctx context.Context, processID string) (*SigmaProcess, error) {
	row := q.queryRow(ctx, q.processGetPIDStmt, processGetPID, processID)
	var i SigmaProcess
	err := row.Scan(
		&i.ID,
		&i.ProcessID,
		&i.VreemdelingID,
		&i.Type,
		&i.Status,
		&i.CreatedAt,
		&i.DeletedAt,
	)
	return &i, err
}

const processUpdate = `-- name: ProcessUpdate :exec
UPDATE sigma.processes
SET status=$1
WHERE process_id=$2
`

type ProcessUpdateParams struct {
	Status    string
	ProcessID string
}

func (q *Queries) ProcessUpdate(ctx context.Context, arg *ProcessUpdateParams) error {
	_, err := q.exec(ctx, q.processUpdateStmt, processUpdate, arg.Status, arg.ProcessID)
	return err
}

const processesGet = `-- name: ProcessesGet :many
SELECT id, process_id, vreemdeling_id, type, status, created_at, deleted_at
FROM sigma.processes
`

func (q *Queries) ProcessesGet(ctx context.Context) ([]*SigmaProcess, error) {
	rows, err := q.query(ctx, q.processesGetStmt, processesGet)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*SigmaProcess{}
	for rows.Next() {
		var i SigmaProcess
		if err := rows.Scan(
			&i.ID,
			&i.ProcessID,
			&i.VreemdelingID,
			&i.Type,
			&i.Status,
			&i.CreatedAt,
			&i.DeletedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const processesGetVreemdelingId = `-- name: ProcessesGetVreemdelingId :many
SELECT id, process_id, vreemdeling_id, type, status, created_at, deleted_at
FROM sigma.processes
WHERE vreemdeling_id=$1
`

func (q *Queries) ProcessesGetVreemdelingId(ctx context.Context, vreemdelingID string) ([]*SigmaProcess, error) {
	rows, err := q.query(ctx, q.processesGetVreemdelingIdStmt, processesGetVreemdelingId, vreemdelingID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*SigmaProcess{}
	for rows.Next() {
		var i SigmaProcess
		if err := rows.Scan(
			&i.ID,
			&i.ProcessID,
			&i.VreemdelingID,
			&i.Type,
			&i.Status,
			&i.CreatedAt,
			&i.DeletedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
