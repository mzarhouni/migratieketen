BEGIN TRANSACTION;

CREATE TABLE bvv_backend.samenvoeging (
    id UUID NOT NULL,
    vreemdeling_leidend_id TEXT NOT NULL,
    vreemdeling_vervallen_id TEXT NOT NULL,

    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    
    PRIMARY KEY(id)
);


COMMIT;
