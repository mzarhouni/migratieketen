FROM digilabpublic.azurecr.io/golang:1.21.5-alpine3.19 as builder

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api/go.mod fictief-bvv-api/go.sum ./

WORKDIR /build/fictief-bvv-backend
COPY fictief-bvv-backend/go.mod fictief-bvv-backend/go.sum ./

RUN go mod download

## Build the Go Files
WORKDIR /build/fictief-bvv-api
COPY fictief-bvv-api ./

WORKDIR /build/fictief-bvv-backend
COPY fictief-bvv-backend ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o /build/server

## Run the server for dev
ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server serve"


FROM digilabpublic.azurecr.io/alpine:3.19

# Add timezones
RUN apk add --no-cache tzdata

# Copy the bin from builder to root.
COPY --from=builder /build/server /app/server

WORKDIR /app

ENTRYPOINT ["/app/server", "serve"]

EXPOSE 8080
