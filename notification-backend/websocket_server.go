package main

import (
	"context"
	"encoding/json"
	"log/slog"
	"net/http"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"nhooyr.io/websocket"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type WebsocketServer struct {
	http.Server
	sockets             sync.Map
	logger              *slog.Logger
	notificationChannel chan []byte
	cfg                 *Config
}

func NewWebsocketServer(config *Config, logger *slog.Logger) *WebsocketServer {
	logger.Info("Starting server on", "addr", config.ServerAddress)

	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)

	ws := WebsocketServer{
		Server: http.Server{
			Addr: config.ServerAddress,
		},
		sockets:             sync.Map{},
		logger:              logger,
		notificationChannel: make(chan []byte),
		cfg:                 config,
	}

	router.Mount("/v0", api.Handler(&ws))

	ws.Server.Handler = router
	return &ws
}

func (s *WebsocketServer) Run() {
	go func() {
		err := s.ListenAndServe()
		s.logger.Error("server encountered an error", "err", err)
	}()
}

// PublishNotification implements api.ServerInterface.
func (s *WebsocketServer) PublishNotification(w http.ResponseWriter, r *http.Request) {
	notification := &api.NotificationPublishRequest{}
	if err := json.NewDecoder(r.Body).Decode(notification); err != nil {
		s.SendErrorResponse(w, err, "could not parse notification request", 400)
		return
	}

	notificationData, err := json.Marshal(notification.Data)
	if err != nil {
		s.SendErrorResponse(w, err, "could not encode notification data", 400)
		return
	}

	// Publish notification
	s.notificationChannel <- notificationData

	// Respond
	w.WriteHeader(http.StatusNoContent)
}

// SubscribeNotification implements api.ServerInterface.
func (s *WebsocketServer) SubscribeNotification(w http.ResponseWriter, r *http.Request) {
	s.logger.Info("Recieved websocket connection")

	// TODO: Remove insecure
	connection, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		InsecureSkipVerify: true,
	})

	if err != nil {
		s.SendErrorResponse(w, err, "could not setup websocket connection", 500)
		return
	}

	s.sockets.Store(r.RemoteAddr, connection)
	go s.readMessages(r.RemoteAddr, connection)
}

func (s *WebsocketServer) SendErrorResponse(w http.ResponseWriter, err error, message string, code int) {
	s.logger.Error(message, "err", err)

	w.WriteHeader(code)
	_, err = w.Write([]byte(err.Error()))
	if err != nil {
		s.logger.Error("Failed to send error response", "err", err)
	}
}

func (s *WebsocketServer) PushNotification(notification string) {
	s.sockets.Range(func(key, value any) bool {
		connection := value.(*websocket.Conn)
		err := connection.Write(context.Background(), websocket.MessageText, []byte(notification))
		if err != nil {
			s.logger.Error("failed to write", "err", err, "dest", key)
			s.sockets.Delete(key)
		}
		return true
	})
}

func (s *WebsocketServer) readMessages(key any, connection *websocket.Conn) {
	for {
		_, msgData, err := connection.Read(context.Background())
		if err != nil {
			s.logger.Error("error while reading websocket", "err", err, "key", key)
			s.sockets.Delete(key)
			break
		}
		s.logger.Warn("Got websocket message, discarding", "source", key, "msg", string(msgData))
	}
}

func (s *WebsocketServer) Shutdown() error {
	err := s.Server.Shutdown(context.Background())
	if err != nil {
		return err
	}

	s.sockets.Range(func(key, value any) bool {
		connection := value.(*websocket.Conn)
		err := connection.Close(http.StatusGone, "shutting down")
		if err != nil {
			s.logger.Error("failed to close socket", "err", err, "dest", key)
			s.sockets.Delete(key)
		}
		return true
	})

	return nil
}
