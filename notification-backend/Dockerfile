FROM digilabpublic.azurecr.io/golang:1.21.5-alpine3.19 as builder

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

WORKDIR /build/notification-api
COPY notification-api/go.mod notification-api/go.sum ./

WORKDIR /build/notification-backend
COPY notification-backend/go.mod notification-backend/go.sum ./

RUN go mod download

## Build the Go Files
WORKDIR /build/config
COPY config ./

WORKDIR /build/notification-api
COPY notification-api ./

WORKDIR /build/notification-backend
COPY notification-backend ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o /build/server

## Run the server for dev
ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server"


FROM digilabpublic.azurecr.io/alpine:3.19

# Add timezones
RUN apk add --no-cache tzdata

# Copy config
COPY config /config

# Copy the bin from builder to root.
COPY --from=builder /build/server /app/server

WORKDIR /app

ENTRYPOINT ["/app/server"]

EXPOSE 8080
